Sujet : Implémenter 

https://www.figma.com/file/89RorhYJu6wRnUDdiKvEYQ/Annuaire-POEI?node-id=0%3A1 en utilisant l’API : Random User Generator

-> La doc de l’API (https://randomuser.me/documentation) se base sur jquery dans l’exemple “How to use”. Vous pouvez ignorer cette partie et l’appeler exactement de la même façon que dummyJson dans le TP précédent.

-> Comportement attendu: quand je clique sur le bouton “ajouter un utilisateur” ou bien sur le bouton “+”, une nouvelle carte avec les informations d’un utilisateur aléatoire récupéré via API se crée.	

-> Pris en compte dans l'évaluation : 

    ->fonctionnement ok et visuel ok, 
    ->code clair (ne pas hésiter à ajouter des commentaires si besoin).

-> Bonus : Factorisation/optimisation, gestion d’erreur.
Pas évalué : Syntaxe “moderne” (ES6, etc).
